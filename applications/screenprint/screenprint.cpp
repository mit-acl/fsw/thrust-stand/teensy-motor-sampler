/**
 * @file screenprint.cpp
 * @brief Example application that dumps teensy motor sample data to terminal
 * @author Parker Lusk <plusk@mit.edu>
 * @date 2 Apr 2021
 */

#include <chrono>
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <memory>
#include <thread>
#include <sstream>

#include <tms/serial_driver.h>

#include "csv.hpp"

class ScreenPrinter
{
public:
  ScreenPrinter(const std::string& port, bool makecsv, const std::string& csvfile)
  : driver_(port), of_(csvfile), writer_(csv::make_csv_writer(of_))
  {
    driver_.registerCallbackSample(std::bind(&ScreenPrinter::callback, this, std::placeholders::_1));
  }
  ~ScreenPrinter() = default;

private:
  uint32_t last_t_us_ = 0;
  acl::tms::SerialDriver driver_;
  std::ofstream of_;
  csv::CSVWriter<std::ofstream> writer_;

  /**
   * @brief      Handles Sample messages when received via serial
   *
   * @param[in]  msg   The unpacked Sample message
   */
  void callback(const tms_serial_sample_msg_t& msg)
  {
    const double dt = (msg.t_us - last_t_us_) * 1e-6; // us to s
    const double hz = 1. / dt;
    last_t_us_ = msg.t_us;

    static constexpr int w = 5;
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2)
       << std::setw(w) << std::setfill(' ')
       << static_cast<int>(msg.motor) << ", "
       << std::setw(w) << std::setfill(' ')
       << msg.usec << ", "
       << std::setw(w) << std::setfill(' ')
       << msg.usecm << ", "
       << std::setw(w) << std::setfill(' ')
       << msg.rpm << ", "
       << std::setw(w) << std::setfill(' ')
       << msg.rpm / 60.;

    std::cout << "Got sample at " << msg.t_us << " us (" << hz << " Hz): "
              << ss.str() << std::endl;

    // immediately write to file
    writer_ << std::vector<double>({ msg.t_us*1e-6, static_cast<double>(msg.usec), static_cast<double>(msg.usecm), static_cast<double>(msg.rpm) });
  }

};

int main(int argc, char const *argv[])
{
  std::string port = "/dev/ttyACM0";

  if (argc == 2) port = std::string(argv[1]);

  ScreenPrinter sp(port, true, "/tmp/motordata.csv");

  // spin forever and let CPU do other things (no busy waiting)
  std::promise<void>().get_future().wait();
  return 0;
}

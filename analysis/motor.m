data = csvread('/tmp/motordata_sfpro60s.csv');

% shift time to zero
data(:,1) = data(:,1) - data(1,1);

% find the sampling dt
dts = diff(data(:,1));
typicaldt = mode(dts);
% due to a sampling (csvwriter?) error, there are some weird jumps
idx = abs(dts-typicaldt) < eps;
data(~idx,:) = [];

figure(1), clf; grid on; hold on;
plot(data(:,1), data(:,2));

% remove hz outliers (ones that have too big a change)
% data(:,3) = data(:,3) - 17;
dhzs = diff(data(:,3));
idx = abs(dhzs) < std(dhzs);
plot(data(idx,1), smoothdata(data(idx,3), 'movmean', 10),'k--','LineWidth',2);
scatter(data(idx,1), data(idx,3),30, 'b.');

% plot a lines on edges of 2000 usec cmd
dd = diff(data(:,2)>=2000);
I = find(dd);
ddI = dd(I);
ddI(ddI<0) = 0;
I = I + ddI;
% add a line for 110 usec (motor on)
L = find(abs(data(:,2) - 1110) < 2);
I = [L(1); I];
for i = 1:length(I)
    xline(data(I(i),1));
end

xlabel('Time [s]');
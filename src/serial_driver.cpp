/**
 * @file serial_driver.cpp
 * @brief Serial communication driver for teensy motor sampler
 * @author Parker Lusk <plusk@mit.edu>
 * @date 2 Apr 2021
 */

#include <iostream>
#include <stdexcept>

#include <async_comm/serial.h>

#include "tms/serial_driver.h"

namespace acl {
namespace tms {

SerialDriver::SerialDriver(const std::string& port, uint32_t baud)
{
  using namespace std::placeholders;

  serial_.reset(new async_comm::Serial(port, baud));
  serial_->register_receive_callback(
            std::bind(&SerialDriver::callback, this, _1, _2));

  if (!serial_->init()) {
    throw std::runtime_error("Could not open serial port '" + port + "'");
  }
}

// ----------------------------------------------------------------------------

SerialDriver::~SerialDriver()
{
  serial_->close();
}

// ----------------------------------------------------------------------------
// Callback stuff
// ----------------------------------------------------------------------------

void SerialDriver::registerCallbackSample(CallbackSample cb)
{
  std::lock_guard<std::mutex> lock(mtx_);
  cb_sample_ = cb;
}

// ----------------------------------------------------------------------------

void SerialDriver::unregisterCallbacks()
{
  std::lock_guard<std::mutex> lock(mtx_);
  cb_sample_ = nullptr;
}


// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void SerialDriver::callback(const uint8_t * data, size_t len)
{
  tms_serial_message_t msg;
  for (size_t i=0; i<len; ++i) {
    if (tms_serial_parse_byte(data[i], &msg)) {

      switch (msg.type) {
        case TMS_SERIAL_MSG_SAMPLE:
          handleSampleMsg(msg);
          break;
      }

    }
  }
}

// ----------------------------------------------------------------------------

void SerialDriver::handleSampleMsg(const tms_serial_message_t& msg)
{
  tms_serial_sample_msg_t sample;
  tms_serial_sample_msg_unpack(&sample, &msg);

  std::lock_guard<std::mutex> lock(mtx_);
  if (cb_sample_) cb_sample_(sample);
}

} // ns tms
} // ns acl
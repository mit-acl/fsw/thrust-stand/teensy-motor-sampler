#include "tms_serial.h"

//=============================================================================
// pin definitions
//=============================================================================

static constexpr int LED_PIN = 13; // on-board teensy LED
static constexpr int ISR_PIN = 23; // photodetector
static constexpr int PWM_PIN = 2;  // pwm sense pin (input)

//=============================================================================
// configuration options
//=============================================================================

// sensor polling interval (micros)
static constexpr int SENSOR_POLL_INTERVAL_US = 5000;

// number of triggers used to estimate optical RPM speed
static constexpr int N_OPT_TRIG = 3;

// which motor to sample
static constexpr int motoridx = 0;

// number of reflective strips on the motor bell
static constexpr int divisor = 1;

// pwm sensing samples for averaging
static constexpr int PWM_SENSING_SAMPLES = 1;

//=============================================================================
// global variables
//=============================================================================

// serial stuff
uint8_t out_buf[TMS_SERIAL_MAX_MESSAGE_LEN];
tms_serial_message_t msg_buf;

// timing
uint32_t sensor_poll_previous_us = 0;
uint32_t start_time_us = 0;

// RPM sensor related 
volatile uint8_t state = LOW;
volatile uint8_t rpm_time_idx = 0;
volatile uint32_t rpm_time[N_OPT_TRIG];
uint32_t rpm_hz_sample_sum = 0;
uint32_t rpm_hz_sample_num = 0;

// PWM sensing
volatile uint32_t pwm_time_on = 0;  // time rising edge detected
volatile uint8_t pwm_time_idx = 0;
volatile uint32_t pwm_time[PWM_SENSING_SAMPLES]; // measured PWM usec

// latest motor cmd from autopilot
volatile uint32_t motor_usec = 0;

//=============================================================================
// Helper functions
//=============================================================================

/**
 * If Data Terminal Ready (DTR) is not high (i.e., no device connected),
 * then the serialEvent may fire and read back what was wrote one byte at
 * a time. This is undesirable as it will put the parser in an unknown state
 * (or something else breaks? not sure exactly). This function checks that
 * there is a device listening for USB serial before writing. If not, the data
 * is simply thrown away.
 */
bool safe_serial_write(const uint8_t* buf, size_t len)
{
  if (Serial.dtr()) {
    Serial.write(buf, len);
    return true;
  } else {
    return false;
  }
}

// ----------------------------------------------------------------------------

void estimate_rpm()
{
  uint32_t t1, t0;
  noInterrupts();
  t0 = rpm_time[(rpm_time_idx + N_OPT_TRIG + 1) % N_OPT_TRIG]; // oldest
  t1 = rpm_time[rpm_time_idx]; // newest
  interrupts();
  float duration = t1 - t0;
  float signal_hz = 0;
  if ((micros() - t1) > 100000 * N_OPT_TRIG){ // If the interrupt has not been triggered in the last 0.5s
    signal_hz = 0;
  } else
  if (duration != 0) { // Prevent division by 0.
    signal_hz = 1e6 * ((float)N_OPT_TRIG-1) / (float)duration;
  } else signal_hz = 0;

  // accumulate
  rpm_hz_sample_sum += signal_hz;
  rpm_hz_sample_num += 1;
}

// ----------------------------------------------------------------------------

uint32_t averaged_rpm()
{
  float avg_hz = static_cast<float>(rpm_hz_sample_sum) / rpm_hz_sample_num;

  // divide by number of strips of tape for one revolution
  avg_hz /= divisor;

  // reset accumulator
  rpm_hz_sample_sum = 0;
  rpm_hz_sample_num = 0;

  return static_cast<uint32_t>(avg_hz) * 60;
}

// ----------------------------------------------------------------------------

uint16_t averaged_pwmread()
{
  float avg_pwm = 0.;
  for (int i=0; i<PWM_SENSING_SAMPLES; i++) {
    avg_pwm += pwm_time[i];
  }

  return static_cast<uint16_t>(avg_pwm / PWM_SENSING_SAMPLES);

}

//=============================================================================
// initialize
//=============================================================================

void setup()
{
  // set up serial communication
  // baud doesn't really matter since USB
  // (make sure selected in menu: Tools > USB Type > Serial)
  Serial.begin(115200);

  // UART from snapdragon for motor cmds (pins 0, 1)
  Serial1.begin(921600);

  // Setup led pin
  pinMode(LED_PIN, OUTPUT);

  // Setup PWM sensing
  pinMode(PWM_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PWM_PIN), isr_pwmread, CHANGE);

  // Setup pin and ISR for RPM photodetector sensor
  pinMode(ISR_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ISR_PIN), isr_encoder, CHANGE);

}

//=============================================================================
// loop
//=============================================================================

void loop()
{

  //
  // RPM Calculation
  //

  estimate_rpm();
  digitalWrite(LED_PIN, state); // visual only useful for slow speeds

  //
  // Data sampling and communication with host
  //

  uint32_t current_time_us = micros() - start_time_us;
 
  if (current_time_us >= sensor_poll_previous_us + SENSOR_POLL_INTERVAL_US) {

    tms_serial_sample_msg_t sample;
    sample.t_us = current_time_us;
    sample.motor = motoridx;
    sample.usec = motor_usec;
    sample.usecm = averaged_pwmread();
    sample.rpm = averaged_rpm();
   
    const size_t len = tms_serial_sample_msg_send_to_buffer(out_buf, &sample);
    safe_serial_write(out_buf, len);

    sensor_poll_previous_us = current_time_us;
  }
}

//=============================================================================
// RPM sensor ISR
//=============================================================================

void isr_pwmread() {
  static uint8_t last = LOW;
  uint8_t curr = digitalReadFast(PWM_PIN);
  if (curr != last) {
    if (curr > last) { // rising edge
      pwm_time_on = micros();
    } else { // falling edge
      pwm_time_idx++;
      pwm_time_idx %= PWM_SENSING_SAMPLES;
      pwm_time[pwm_time_idx] = micros() - pwm_time_on;
    }
    last = curr;
  }
}

//=============================================================================
// RPM sesnor ISR
//=============================================================================

void isr_encoder() {
  state = digitalReadFast(ISR_PIN);
  if (state == HIGH)  { // rising edge
    rpm_time_idx++;
    rpm_time_idx %= N_OPT_TRIG;
    rpm_time[rpm_time_idx] = micros();
  }
}

//=============================================================================
// handle received serial data (from autopilot)
//=============================================================================

void serialEvent1()
{
  while (Serial1.available()) {
    uint8_t in_byte = (uint8_t) Serial1.read();
    if (tms_serial_parse_byte(in_byte, &msg_buf)) {
      switch (msg_buf.type) {
        case TMS_SERIAL_MSG_MOTORCMD:
        {
          tms_serial_motorcmd_msg_t msg;
          tms_serial_motorcmd_msg_unpack(&msg, &msg_buf);
          handle_motorcmd_msg(msg);
          break;
        }
      }
    }
  }
}

//=============================================================================
// handle received serial data (from host)
//=============================================================================

void serialEvent()
{
  // while (Serial.available()) {
  //   uint8_t in_byte = (uint8_t) Serial.read();
  //   if (tms_serial_parse_byte(in_byte, &msg_buf)) {
  //     switch (msg_buf.type) {
        
  //     }
  //   }
  // }
}

//=============================================================================
// handle received messages
//=============================================================================

void handle_motorcmd_msg(const tms_serial_motorcmd_msg_t& msg)
{
  motor_usec = msg.usec[motoridx];
}

/**
 * @file tms_serial.h
 * @brief Serial protocol for ACL Teensy Motor Sampler
 * @author Parker Lusk <plusk@mit.edu>
 * @date 2 Apr 2021
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

//=============================================================================
// message types
//=============================================================================

typedef enum {
  TMS_SERIAL_MSG_MOTORCMD,
  TMS_SERIAL_MSG_SAMPLE,
  TMS_SERIAL_NUM_MSGS
} tms_msg_type_t;

//=============================================================================
// message definitions
//=============================================================================

typedef struct {
  uint16_t usec[6]; // per motor pwm cmd in microseconds
} tms_serial_motorcmd_msg_t;

typedef struct {
  uint32_t t_us; // time in microseconds
  uint8_t motor; // motor number being sampled
  uint16_t usec; // motor pwm cmd in microseconds
  uint16_t usecm; // motor pwm cmd *measured* in microseconds
  uint32_t rpm; // measured rpm of motor
} tms_serial_sample_msg_t;

// payload lengths
static const float TMS_SERIAL_PAYLOAD_LEN[] = {
  sizeof(tms_serial_motorcmd_msg_t),
  sizeof(tms_serial_sample_msg_t),
};

// manually indicate the largest msg payload
static const size_t TMS_SERIAL_MAX_PAYLOAD_LEN = sizeof(tms_serial_sample_msg_t);

//=============================================================================
// generic message type
//=============================================================================

static const uint8_t TMS_SERIAL_MAGIC = 0xA5;

typedef struct __attribute__((__packed__)) {
  uint8_t magic;
  uint8_t type;
  uint8_t payload[TMS_SERIAL_MAX_PAYLOAD_LEN];
  uint8_t crc;
} tms_serial_message_t;

static const size_t TMS_SERIAL_MAX_MESSAGE_LEN = sizeof(tms_serial_message_t);

//=============================================================================
// utility functions
//=============================================================================

// source: http://www.nongnu.org/avr-libc/user-manual/group__util__crc.html#gab27eaaef6d7fd096bd7d57bf3f9ba083
uint8_t tms_serial_update_crc(uint8_t inCrc, uint8_t inData)
{
  uint8_t i;
  uint8_t data;

  data = inCrc ^ inData;

  for ( i = 0; i < 8; i++ )
  {
    if (( data & 0x80 ) != 0 )
    {
      data <<= 1;
      data ^= 0x07;
    }
    else
    {
      data <<= 1;
    }
  }
  return data;
}

void tms_serial_finalize_message(tms_serial_message_t *msg)
{
  msg->magic = TMS_SERIAL_MAGIC;

  uint8_t crc = 0;
  crc = tms_serial_update_crc(crc, msg->magic);
  crc = tms_serial_update_crc(crc, msg->type);
  for (size_t i=0; i<TMS_SERIAL_PAYLOAD_LEN[msg->type]; ++i)
  {
    crc = tms_serial_update_crc(crc, msg->payload[i]);
  }

  msg->crc = crc;
}

size_t tms_serial_send_to_buffer(uint8_t *dst, const tms_serial_message_t *src)
{
  size_t offset = 0;
  memcpy(dst + offset, &src->magic,  sizeof(src->magic)); offset += sizeof(src->magic);
  memcpy(dst + offset, &src->type,   sizeof(src->type));  offset += sizeof(src->type);
  memcpy(dst + offset, src->payload, TMS_SERIAL_PAYLOAD_LEN[src->type]); offset += TMS_SERIAL_PAYLOAD_LEN[src->type];
  memcpy(dst + offset, &src->crc,    sizeof(src->crc)); offset += sizeof(src->crc);
  return offset;
}

//=============================================================================
// Motor command message
//=============================================================================

void tms_serial_motorcmd_msg_pack(tms_serial_message_t *dst, const tms_serial_motorcmd_msg_t *src)
{
  dst->type = TMS_SERIAL_MSG_MOTORCMD;
  size_t offset = 0;
  memcpy(dst->payload + offset, &src->usec, sizeof(src->usec)); offset += sizeof(src->usec);
  tms_serial_finalize_message(dst);
}

void tms_serial_motorcmd_msg_unpack(tms_serial_motorcmd_msg_t *dst, const tms_serial_message_t *src)
{
  size_t offset = 0;
  memcpy(&dst->usec, src->payload + offset, sizeof(dst->usec)); offset += sizeof(dst->usec);
}

size_t tms_serial_motorcmd_msg_send_to_buffer(uint8_t *dst, const tms_serial_motorcmd_msg_t *src)
{
  tms_serial_message_t msg;
  tms_serial_motorcmd_msg_pack(&msg, src);
  return tms_serial_send_to_buffer(dst, &msg);
}

//=============================================================================
// Sample message
//=============================================================================

void tms_serial_sample_msg_pack(tms_serial_message_t *dst, const tms_serial_sample_msg_t *src)
{
  dst->type = TMS_SERIAL_MSG_SAMPLE;
  size_t offset = 0;
  memcpy(dst->payload + offset, &src->t_us, sizeof(src->t_us)); offset += sizeof(src->t_us);
  memcpy(dst->payload + offset, &src->motor, sizeof(src->motor)); offset += sizeof(src->motor);
  memcpy(dst->payload + offset, &src->usec, sizeof(src->usec)); offset += sizeof(src->usec);
  memcpy(dst->payload + offset, &src->usecm, sizeof(src->usecm)); offset += sizeof(src->usecm);
  memcpy(dst->payload + offset, &src->rpm, sizeof(src->rpm)); offset += sizeof(src->rpm);
  tms_serial_finalize_message(dst);
}

void tms_serial_sample_msg_unpack(tms_serial_sample_msg_t *dst, const tms_serial_message_t *src)
{
  size_t offset = 0;
  memcpy(&dst->t_us, src->payload + offset, sizeof(dst->t_us)); offset += sizeof(dst->t_us);
  memcpy(&dst->motor, src->payload + offset, sizeof(dst->motor)); offset += sizeof(dst->motor);
  memcpy(&dst->usec, src->payload + offset, sizeof(dst->usec)); offset += sizeof(dst->usec);
  memcpy(&dst->usecm, src->payload + offset, sizeof(dst->usecm)); offset += sizeof(dst->usecm);
  memcpy(&dst->rpm, src->payload + offset, sizeof(dst->rpm)); offset += sizeof(dst->rpm);
}

size_t tms_serial_sample_msg_send_to_buffer(uint8_t *dst, const tms_serial_sample_msg_t *src)
{
  tms_serial_message_t msg;
  tms_serial_sample_msg_pack(&msg, src);
  return tms_serial_send_to_buffer(dst, &msg);
}

//==============================================================================
// parser
//==============================================================================

typedef enum
{
  TMS_SERIAL_PARSE_STATE_IDLE,
  TMS_SERIAL_PARSE_STATE_GOT_MAGIC,
  TMS_SERIAL_PARSE_STATE_GOT_TYPE,
  TMS_SERIAL_PARSE_STATE_GOT_PAYLOAD
} tms_serial_parse_state_t;

bool tms_serial_parse_byte(uint8_t byte, tms_serial_message_t *msg)
{
  static tms_serial_parse_state_t parse_state = TMS_SERIAL_PARSE_STATE_IDLE;
  static uint8_t crc_value = 0;
  static size_t payload_index = 0;
  static tms_serial_message_t msg_buffer;

  bool got_message = false;
  switch (parse_state)
  {
  case TMS_SERIAL_PARSE_STATE_IDLE:
    if (byte == TMS_SERIAL_MAGIC)
    {
      crc_value = 0;
      payload_index = 0;

      msg_buffer.magic = byte;
      crc_value = tms_serial_update_crc(crc_value, byte);

      parse_state = TMS_SERIAL_PARSE_STATE_GOT_MAGIC;
    }
    break;

  case TMS_SERIAL_PARSE_STATE_GOT_MAGIC:
    msg_buffer.type = byte;
    crc_value = tms_serial_update_crc(crc_value, byte);
    parse_state = TMS_SERIAL_PARSE_STATE_GOT_TYPE;
    break;

  case TMS_SERIAL_PARSE_STATE_GOT_TYPE:
    msg_buffer.payload[payload_index++] = byte;
    crc_value = tms_serial_update_crc(crc_value, byte);
    if (payload_index == TMS_SERIAL_PAYLOAD_LEN[msg_buffer.type])
    {
      parse_state = TMS_SERIAL_PARSE_STATE_GOT_PAYLOAD;
    }
    break;

  case TMS_SERIAL_PARSE_STATE_GOT_PAYLOAD:
    msg_buffer.crc = byte;
    if (msg_buffer.crc == crc_value)
    {
      got_message = true;
      memcpy(msg, &msg_buffer, sizeof(msg_buffer));
    }
    parse_state = TMS_SERIAL_PARSE_STATE_IDLE;
    break;
  }

  return got_message;
}

/**
 * @file serial_driver.h
 * @brief Serial communication driver for teensy motor sampler
 * @author Parker Lusk <plusk@mit.edu>
 * @date 2 Apr 2021
 */

#include <cstdint>
#include <functional>
#include <memory>
#include <mutex>
#include <string>

#include "protocol/tms_serial.h"

namespace async_comm { class Serial; }

namespace acl {
namespace tms {

  using CallbackSample = std::function<void(const tms_serial_sample_msg_t&)>;

  class SerialDriver
  {
  public:
    SerialDriver(const std::string& port = "/dev/ttyACM0", uint32_t baud = 115200);
    ~SerialDriver();

    void registerCallbackSample(CallbackSample cb);
    void unregisterCallbacks();
    
  private:
    std::unique_ptr<async_comm::Serial> serial_;
    CallbackSample cb_sample_;
    std::mutex mtx_; ///< synchronize callback resource reg/unreg

    void callback(const uint8_t * data, size_t len);

    void handleSampleMsg(const tms_serial_message_t& msg);
  };

} // ns tms
} // ns acl